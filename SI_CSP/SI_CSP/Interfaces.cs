﻿using System.Collections.Generic;

namespace SI_CSP
{
    public interface IProblem
    {
        bool IsSolved { get; }
        bool IsValid { get; }
        int Size { get; }
        int SquaredSize { get; }
    }

    public interface IBacktrackingProblem : IProblem
    {
        List<IBacktrackingProblem> GetBacktrackingExtensionsFor(int _index);
    }

    public interface IForwardCheckingProblem : IProblem
    {
        List<IForwardCheckingProblem> GetForwardCheckingExtensionsFor(int _index);
    }
}