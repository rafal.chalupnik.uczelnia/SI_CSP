﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SI_CSP
{
    public class LatinSquareProblem : ICloneable, IBacktrackingProblem, IForwardCheckingProblem
    {
        private int[,] board_;

        private LatinSquareProblem() { }

        private bool ColumnContains(int _column, int _value)
        {
            for (var i = 0; i < Size; i++)
            {
                if (board_[i, _column] == _value)
                    return true;
            }

            return false;
        }
        private int FindFirstUnsolvedElementIndex()
        {
            var index = 0;
            var foundZero = false;

            while (!foundZero && index < SquaredSize)
            {
                if (board_[index / Size, index % Size] == 0)
                    foundZero = true;
                else
                    index++;
            }

            return foundZero ? index : -1;
        }
        private List<T> GetExtensions<T>(bool _forwardChecking, int _index) where T : class
        {
            var extensions = new List<T>();
            var row = _index / Size;
            var column = _index % Size;

            for (var i = 1; i <= Size; i++)
            {
                if (!_forwardChecking || (!RowContains(row, i) && !ColumnContains(column, i)))
                {
                    var possibility = Clone() as LatinSquareProblem ?? throw new NullReferenceException();
                    possibility.board_[row, column] = i;
                    extensions.Add(possibility as T);
                }
            }

            return extensions;
        }
        private bool RowContains(int _row, int _value)
        {
            for (var i = 0; i < Size; i++)
            {
                if (board_[_row, i] == _value)
                    return true;
            }

            return false;
        }

        public bool IsSolved => FindFirstUnsolvedElementIndex() == -1;
        public bool IsValid
        {
            get
            {
                var rows = new List<int>[Size];
                for (var i = 0; i < Size; i++)
                    rows[i] = new List<int>();

                var columns = new List<int>[Size];
                for (var i = 0; i < Size; i++)
                    columns[i] = new List<int>();
                
                for (var i = 0; i < SquaredSize; i++)
                {
                    var rowIndex = i / Size;
                    var columnIndex = i % Size;
                    var element = board_[rowIndex, columnIndex];
                    if (element != 0)
                    {
                        rows[rowIndex].Add(element);
                        columns[columnIndex].Add(element);
                    }
                }

                for (var i = 0; i < Size; i++)
                {
                    var row = rows[i];
                    var column = columns[i];

                    if (row.Count != row.Distinct().Count()
                        || column.Count != column.Distinct().Count())
                        return false;
                }

                return true;
            }
        }
        public int Size { get; private set; }
        public int SquaredSize { get; private set; }
        
        public LatinSquareProblem(int _size)
        {
            if (_size <= 0)
                throw new ArgumentException("Size must be greater than 0!");

            Size = _size;
            SquaredSize = Size * Size;
            board_ = new int[Size, Size];
        }

        public object Clone()
        {
            return new LatinSquareProblem()
            {
                board_ = Utils.Copy2DArray(board_),
                Size = Size,
                SquaredSize = SquaredSize
            };
        }
        public List<IBacktrackingProblem> GetBacktrackingExtensionsFor(int _index)
        {
            return GetExtensions<IBacktrackingProblem>(false, _index);
        }
        public List<IForwardCheckingProblem> GetForwardCheckingExtensionsFor(int _index)
        {
            return GetExtensions<IForwardCheckingProblem>(true, _index);
        }
        public void LoadFromString(string _string)
        {
            if (string.IsNullOrEmpty(_string))
                throw new ArgumentNullException(nameof(_string), "String cannot be empty!");
            if (_string.Length != SquaredSize)
                throw new ArgumentException("Invalid string size!");

            for (var i = 0; i < SquaredSize; i++)
                board_[i / Size, i % Size] = (int) char.GetNumericValue(_string[i]);
        }
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();

            for (var r = 0; r < Size; r++)
            {
                for (var c = 0; c < Size; c++)
                {
                    stringBuilder.Append(board_[r, c]);
                    stringBuilder.Append("\t");
                }
                stringBuilder.Append("\n");
            }

            return stringBuilder.ToString();
        }
    }
}