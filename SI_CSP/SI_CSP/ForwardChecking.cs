﻿using System.Collections.Generic;
using System.Linq;

namespace SI_CSP
{
    public static class ForwardChecking
    {
        public static IForwardCheckingProblem Run(IForwardCheckingProblem _startProblem, JumpCounter _jumpCounter, List<int> _indexQueue = null)
        {
            if (_indexQueue == null)
                _indexQueue = Enumerable.Range(0, _startProblem.SquaredSize).ToList();

            _jumpCounter.Injumps++;

            if (_startProblem.IsSolved)
            {
                _jumpCounter.Outjumps++;
                return _startProblem;
            }

            var children = _startProblem.GetForwardCheckingExtensionsFor(_indexQueue.First());
            _indexQueue.RemoveAt(0);

            if (children.Count != 0)
            {
                foreach (var child in children)
                {
                    var result = Run(child, _jumpCounter, _indexQueue.ToList());
                    if (result != null)
                    {
                        _jumpCounter.Outjumps++;
                        return result;
                    }
                }
            }

            return null;
        }
    }
}