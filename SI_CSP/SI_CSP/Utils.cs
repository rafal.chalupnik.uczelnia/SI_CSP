﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace SI_CSP
{
    public static class Utils
    {
        public static int[,] Copy2DArray(int[,] _array)
        {
            if (_array == null)
                return null;

            var rows = _array.GetLength(0);
            var columns = _array.GetLength(1);

            var copiedArray = new int[rows, columns];

            for (var r = 0; r < rows; r++)
            {
                for (var c = 0; c < columns; c++)
                    copiedArray[r, c] = _array[r, c];
            }

            return copiedArray;
        }

        public static List<int> GetTwoWayList(int _n)
        {
            var ordered = Enumerable.Range(0, _n).ToList();
            var output = new List<int>();
            var first = false;

            while (ordered.Count != 0)
            {
                var selected = first ? ordered.First() : ordered.Last();
                output.Add(selected);
                ordered.Remove(selected);
                first = !first;
            }

            return output;
        }
        public static string GetStringFrom1ToN(int _n)
        {
            if (_n <= 0)
                return null;
            
            var values = Enumerable.Range(1, _n).ToList();
            var stringBuilder = new StringBuilder();

            for (var i = 0; i < _n; i++)
                stringBuilder.Append(values[i]);

            return stringBuilder.ToString();
        }
        public static List<int> RandomIndexesList(int _n, Random _random)
        {
            var ordered = Enumerable.Range(0, _n).ToList();
            var output = new List<int>();

            while (ordered.Count != 0)
            {
                var selected = ordered[_random.Next(ordered.Count)];
                ordered.Remove(selected);
                output.Add(selected);
            }

            return output;
        }
        public static string RandomString(Random _random, string _characters, int _size)
        {
            if (_random == null)
                throw new ArgumentNullException(nameof(_random), "Random cannot be null!");
            if (string.IsNullOrEmpty(_characters))
                throw new ArgumentNullException(nameof(_characters), "Characters cannot be empty!");
            if (_size <= 0)
                throw new ArgumentException("Size must be greater than 0!");

            return new string(Enumerable.Repeat(_characters, _size).Select(_s => _s[_random.Next(_s.Length)]).ToArray());
        }
    }
}