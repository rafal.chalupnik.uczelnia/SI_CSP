﻿namespace SI_CSP
{
    public class JumpCounter
    {
        public int Injumps { get; set; }
        public int Outjumps { get; set; }

        public override string ToString()
        {
            return $"### Profiler:\n\tInjumps: {Injumps}\n\tOutjumps: {Outjumps}\n";
        }
    }
}