﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace SI_CSP
{
    public class NQueensProblem : ICloneable, IBacktrackingProblem, IForwardCheckingProblem
    {
        private readonly int[] positions_;
        private readonly List<int>[] possibilities_;

        private List<T> GetExtensions<T>(bool _forwardChecking, int _index) where T : class
        {
            var extensions = new List<T>();

            for (var i = 1; i <= Size; i++)
            {
                var clone = Clone() as NQueensProblem;
                if (!_forwardChecking || clone.possibilities_[_index].Contains(i))
                {
                    clone[_index] = i;
                    extensions.Add(clone as T);
                }
            }

            return extensions;
        }

        public NQueensProblem(int _size)
        {
            if (_size <= 0)
                throw new ArgumentException("Size must be greater than 0!");
            Size = _size;
            SquaredSize = _size;

            positions_ = new int[_size];
            possibilities_ = new List<int>[_size];

            var fullPossibilities = Enumerable.Range(1, Size).ToList();
            for (var i = 0; i < Size; i++)
            {
                possibilities_[i] = new List<int>();
                possibilities_[i].AddRange(fullPossibilities);
            }
        }

        public int this[int _row]
        {
            get => positions_[_row];
            set
            {
                if (value == 0)
                    return;

                if (!possibilities_[_row].Contains(value))
                    IsValid = false;

                positions_[_row] = value;
                
                for (var i = 0; i < Size; i++)
                {
                    // Wykreśl w pionie
                    possibilities_[i].Remove(value);

                    // Wykreśl w diagonali
                    var diff = i - _row;
                    possibilities_[i].Remove(value - diff);
                    possibilities_[i].Remove(value + diff);
                }
            }
        }
        public bool IsSolved
        {
            get
            {
                if (!IsValid)
                    return false;

                for (var i = 0; i < Size; i++)
                {
                    if (positions_[i] == 0)
                        return false;
                }

                return true;
            }
        }
        public bool IsValid { get; private set; } = true;
        public int Size { get; }
        public int SquaredSize { get; }

        public object Clone()
        {
            var output = new NQueensProblem(Size);

            for (var i = 0; i < Size; i++)
                output[i] = positions_[i];

            return output;
        }
        public List<IBacktrackingProblem> GetBacktrackingExtensionsFor(int _index)
        {
            return GetExtensions<IBacktrackingProblem>(false, _index);
        }
        public List<IForwardCheckingProblem> GetForwardCheckingExtensionsFor(int _index)
        {
            return GetExtensions<IForwardCheckingProblem>(true, _index);
        }
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();

            for (var i = 0; i < Size; i++)
            {
                var position = positions_[i];

                for (var j = 0; j < Size; j++)
                {
                    stringBuilder.Append("|");
                    stringBuilder.Append(j + 1 == position ? "X" : " ");
                }

                stringBuilder.Append("|");
                stringBuilder.Append("\n");
            }

            return stringBuilder.ToString();
        }
    }
}