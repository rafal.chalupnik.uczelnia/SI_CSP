﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace SI_CSP
{
    public static class Program
    {
        public static void Compare(IProblem _problem)
        {
            var stopwatch = Stopwatch.StartNew();
            var forwardCheckingCounter = new JumpCounter();
            var forwardCheckingResult = ForwardChecking.Run(_problem as IForwardCheckingProblem, forwardCheckingCounter);
            stopwatch.Stop();
            Console.WriteLine($"ForwardChecking:\t{stopwatch.ElapsedMilliseconds}");
            Console.WriteLine(forwardCheckingResult);
            Console.WriteLine(forwardCheckingCounter);
            Console.WriteLine();

            stopwatch = Stopwatch.StartNew();
            var backtrackingCounter = new JumpCounter();
            var backtrackingResult = Backtracking.Run(_problem as IBacktrackingProblem, backtrackingCounter);
            stopwatch.Stop();
            Console.WriteLine($"Backtracking:\t\t{stopwatch.ElapsedMilliseconds}");
            Console.WriteLine(backtrackingResult);
            Console.WriteLine(backtrackingCounter);
            Console.WriteLine();
        }

        private static void MeasureProblem(bool _forwardChecking, IProblem _problem)
        {
            var counter = new JumpCounter();
            var stopwatch = Stopwatch.StartNew();
            IProblem result;
            if (_forwardChecking)
                result = ForwardChecking.Run(_problem as IForwardCheckingProblem, counter);
            else
                result = Backtracking.Run(_problem as IBacktrackingProblem, counter);
            stopwatch.Stop();
            Console.WriteLine($"Time: {stopwatch.ElapsedMilliseconds}");
            Console.WriteLine(counter);
            Console.WriteLine("");
            if (_forwardChecking)
                Console.WriteLine(result);
        }

        public static void Main(string[] _args)
        {
            MeasureProblem(false, new LatinSquareProblem(14));
        }
    }
}